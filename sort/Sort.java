class Sort {

    /**
     * It is possible to improve by boolean variable, which describes, if array is sorted.
     */
    public static int[] bubble(int[] arr) {
        int tmp;
        for(int n = 0; n < arr.length; n++) {
            for(int i = 1; i < (arr.length - n); i++) {
                if(arr[i-1] > arr[i]) {
                    tmp = arr[i-1];
                    arr[i-1] = arr[i];
                    arr[i] = tmp;
                }
            }
        }
        return arr;
    }

    public static void main(String[] args) {

        int[] arr = {3,2,6,1,4,5};

        arr = bubble(arr);
        for(int i = 0; i < arr.length; i++) {
            System.out.printf("%d ", arr[i]);
        }

    }

}