import java.util.Arrays;

/**
 *
 * @author Martin Kusnier
 * @licence CC-BY
 */
public class Matrix {
    
    /**
     * Method itself print given matrix to stdout
     * 
     * @param mat[m][n]
     */
    public static void print(int[][] mat) {
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat[i].length; j++) {
                System.out.printf("%4d ", mat[i][j]);
            }
            System.out.printf("\n\n");
        }
    }
    
    /**
     * Method multiples dimensions of given matrix;
     * 
     * @param mat[m]*[n]
     * @return int 
     */
    public static int countElements(int[][] mat) {
        int retVal = mat[0].length * mat.length;
        
        return retVal;
    }

    /**
     * Counts occurance of elements in array
     * 
     * @param mat int[][]
     * @param min int
     * @param max int
     * @return int[][]
     */
    public static int[][] occurElements(int[][] mat, int min, int max) {
        int[][] retVal = new int[2][max-min+1];
        
        for (int i = min; i <= max; i++) {
            retVal[0][i-min] = i;
            for (int j = 0; j < mat.length; j++) {
                for (int k = 0; k < mat[j].length; k++) {
                    if(mat[j][k] == i) {
                        retVal[1][i-min]++;
                    }
                }
            }        
        }
        
        return retVal;
    }

    /**
     * Counts elements in given matrix. Elements must be from sequence 1 to a
     * 
     * @param mat int[][]
     * @param a int
     * @return int[]
     */
    public static int[] occurElementsInSequence(int[][] mat, int a) {
        int[] retVal = new int[a];
        
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat[i].length; j++) {
                retVal[mat[i][j]-1]++;
            }
        }
        
        return retVal;
    }
    
    /**
     * Determines whether given matricies has equal dimensons
     * 
     * @param matA
     * @param matB
     * @return 
     */
    public static boolean hasSameDimensions(int[][] matA, int[][] matB) {
        if(matA.length != matB.length) {
            return false;
        } else if (matA[0].length != matB[0].length) {
            return false;
        }
        
        return true;
    }
    
    /**
     * Generates an eye matrix
     * 
     * 
     * @param m dimension
     * @return int[m][m]
     */
    public static int[][] generateEye(int m) {
        int[][] retVal = new int[m][m]; 
        
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < m; j++) {
                retVal[i][j] = ( (i==j) ? 1 : 0 );
            }
        }
        
        return retVal;
    }
    
    /**
     * Method generates m x n sized matrix filled up by random numbers higher
     * then x and lower then y
     * 
     * @param m dimension
     * @param n dimension
     * @param min lower range limit
     * @param max upper range limit
     * @return int[][]
     */
    public static int[][] generateRandom(int m, int n, int min, int max) {
        int[][] retVal = new int[m][n];
        
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                retVal[i][j] =  (int) (Math.random() * (max - min + 1) + min);
            }
        }
        
        
        return retVal;
    }
    
    /**
     * Generate square matrix m x m full of zeroes;
     * 
     * @param m dimension
     * @return int[][]
     */
    public static int[][] generateZeros(int m) {
        int[][] retVal = generateZeros(m, m);
        
        return retVal;
    }
    
    /**
     * Generates m x n matrix full of zeroes;
     * 
     * @param m dimension
     * @param n dimension
     * @return int[m][n]
     */
    public static int[][] generateZeros(int m, int n) {
        int[][] retVal = new int[m][n];
        
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                retVal[i][j] = 0;
            }
        }
        
        return retVal;
    }
    
    /**
     * Generates latin square
     * 
     * @param n int
     * @param regular boolean
     * @return int[][]
     */
    public static int[][] generateLatinSquare(int n, boolean regular) {
        int[][] retVal = new int[n][n];
        
        for (int i = 0; i < n; i++) {
            retVal[0][i] = i + 1;
        }
        for (int i = 1; i < n; i++) {
            retVal[i] = rotateVector(retVal[i-1]);
        }
        
        if(regular) {
            retVal = shuffleRows(retVal);
        }
        
        return retVal;
    }
    
    public static int[][] shuffleRows(int[][] mat) {
        int[][] retVal = mat;
        
        int shuffleCount = 
        
        return retVal;
    }
            
    
    /**
     * Checks if given matrix is latin square
     * Latin square is n x n sized matrix filled by numbers from 1 to n included.
     * Also latin square must has every number just once in every column and row.
     * 
     * @param mat[][]
     * @return boolean
     */
    public static boolean isLatinSquare(int[][] mat) {
        if (mat.length != mat[0].length) {
            return false;
        }
        int[][]transposed = transpose(mat);

        for (int i = 0; i < mat.length; i++) {
            Arrays.sort(mat[i]);
            Arrays.sort(transposed[i]);
            for (int j = 0; j < mat[i].length; j++) {
                if (mat[i][j] != j+1 || transposed[i][j] != j+1) {
                    return false;
                }
            }
        }
        return true;
    }
    
    /**
     * Tronspose given matrix
     * 
     * @param mat int[][]
     * @return int[][]
     */
    public static int[][] transpose(int[][] mat) {
        int[][] retVal = new int[mat[0].length][mat.length];
        
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat[i].length; j++) {
                retVal[j][i] = mat[i][j];
            }
        }
    
        return retVal;
    }
    
    /**
     * Method multiples given matrix by scalar
     * 
     * @param x scalar
     * @param mat[m][n]
     * @return int[m][n]
     */
    public static int[][] multiple(int x, int[][] mat) {
        int[][] retVal = mat;
        
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat[i].length; j++) {
                retVal[i][j] = mat[i][j] * x;
            }
        }
        
        return retVal;
    }
    
    /**
     * Multiple two matricies
     * 
     * @param matA 
     * @param matB
     * @return int[][]
     */
    public static int[][] multiple(int[][] matA, int[][] matB) {
        int[][] retVal = new int[matA.length][matB[0].length];
        
        if (matA[0].length != matB.length) {
            System.out.printf("Error: Given matricies has invalid dimensions\n");
            return retVal;
        }
        
        for (int i = 0; i < matA.length; i++) {
            for (int j = 0; j < matB[0].length; j++) {
                for (int k = 0; k < matB.length; k++) {
                    retVal[i][j] += matA[i][k] * matB[k][j];
                }
            }
        }
        
        return retVal;
    }
    
    /**
     * Sum of given matricies
     * 
     * @param matA
     * @param matB
     * @return int[][]
     */
    public static int[][] sum(int[][] matA, int[][] matB) {
        int[][] retVal = new int[matA.length][matA[0].length];
        
        if( ! hasSameDimensions(matA, matB)) {
            System.out.println("Error: Given matricies has invalid dimensions\n");
            return retVal;
        }
        
        for (int i = 0; i < matA.length; i++) {
            for (int j = 0; j < matA[i].length; j++) {
                retVal[i][j] = matA[i][j] + matB[i][j];
            }
        }
    
        return retVal;
    }
    
    /**
     * main method used for debugging
     */
    public static void main(String[] args) {
        System.out.println("Debuging class: MatrixOperations");
        
        //int[][] matice = {{1,4},{2,5},{3,6}};
        //print(transpose(matice));
        
        //int[][] test = {{1,2,3,4},{2,3,4,1},{3,4,1,2},{4,1,2,3}}; // Je
        //int[][] test = {{1,2,3},{2,3,1},{3,1,2}}; //Je
        //int[][] test = {{1,2,3},{4,5,6},{7,8,9}}; // Není
        int[][] test = {{1,2,3},{2,3,1},{1,3,2}}; // Je
        //int[][] test = {{1,2,4,3},{3,1,2,4},{2,4,3,1},{4,3,1,2}};
        //int[][] test = {{1,2,3,4},{4,3,2,1},{3,4,1,2},{2,1,4,3}}; // Je 
//        int[][] test = {{1,2,3,4},{1,2,4,3},{1,3,4,2},{2,4,3,1}};
        //        System.out.printf("%s o latinský čtverec\n", isLatinSquare(test)?"Jedná":"Nejedná");
        
    }
    
} 