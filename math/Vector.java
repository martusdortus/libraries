/**
 *
 * @author Martin Kusnier
 * @licence CC-BY
 */
public class Vector {

    public static void print(int[] mat) {
        for(int i = 0; i < mat.length; i++) {
            System.out.printf("%d ", mat[i]);
        }
        System.out.printf("\n");
    }

    public static int[] reverse(int[] v) {
        int[] retVal = new int[v.length];
        retVal[0] = v[v.length - 1];
        retVal[v.length - 1] = v[0];
        for (int i = 1; i < v.length; i++) {
            retVal[i] = v[v.length - 1 - i];
        }
        return retVal;
    }
    
    public static int[] rotate(int[] v) {
        int[] retVal = new int[v.length];
        for (int i = 1; i < v.length; i++) {
            retVal[i-1] = v[i];
        }
        retVal[v.length -1] = v[0];
        return retVal;
    }
}